new Swiper('.js-main-slider', {
    loop: true,
    autoplay: {
        delay: 8000,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
    navigation: {
        nextEl: ".js-main-slider-next",
        prevEl: ".js-main-slider-prev",
    }
});

new Swiper('.js-tour-item-slider', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
});

new Swiper('.js-experiences', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 40,
    pagination: {
        el: '.js-experiences-swiper-pagination',
        type: 'bullets',
    },
    navigation: {
        nextEl: ".js-experiences-next",
        prevEl: ".js-experiences-prev",
    },
    breakpoints: {
        768: {
            slidesPerView: 2
        },
        1280: {
            slidesPerView: 4
        }
    }
});


document.addEventListener('click', (event)=> {
    if (event.target.closest('.js-open-menu-mobile-btn')) {
        document.querySelector('.js-overlay').classList.add('overlayer_active');
        document.querySelector('.js-mobile-menu').classList.add('mobile-menu_active');
    }
    
    if (event.target.closest('.js-close-menu-mobile-btn')) {
        document.querySelector('.js-overlay').classList.remove('overlayer_active');
        document.querySelector('.js-mobile-menu').classList.remove('mobile-menu_active');
    }
});